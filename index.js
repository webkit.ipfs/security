module.exports = {
   GPG: require('./lib/GPG'),
   OpenSSL: require('./lib/OpenSSL'),
   CredentialsStore: require('./lib/CredentialsStore'),
   CertificateAuthority: require('./lib/CertificateAuthority'),
   SecureRequest: require('./lib/SecureRequest')
}

//* @webkit.ipfs/security modules.