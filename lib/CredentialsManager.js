const fs = require('fs')
const Log = console.log
const Exit = require('node-graceful')

const GPG = require('./GPG')
const BSON = require('bson')
const Index = require('fuse.js')

const Debug = require('debug')
const debug = Debug('security')


const Owner = (path) => JSON.parse(fs.readFileSync(path).toString()).owner

class CredentialsManager {
   #path
   #store
   #credentials
   #index

   #pw
   #lockMessage = `Locked by CredentialsManager process: ${process.pid}
   If no CredentialsManager running, safe to delete associated '.lock' file.`

   constructor (path, { owner=null, pw=null }={}) {
      if (!path) throw "CredentialsManager constructor requires the path to the credentials store."
      this.owner = fs.existsSync(path)
         ? Owner(path)
         : owner || USER
      this.hasLock = false
      this.#pw = pw
      this.#path = path
      this.#store = null
      this.#index = null
      this.#initialize()

      debug(this)
   }


   #createStore() {
      this.#credentials = {}
      this.#store = {
         owner: this.owner,
         created: Date.now(),
         modified: null,
         encrypted: null
      }
      this.#encryptStore()
   }


   #reloadStore () {
      const path = this.#path
      try { this.#store = JSON.parse(fs.readFileSync(path)) }
      catch (error) { throw "Not a credential store." }

      try {
         let { encrypted } = this.#store
         this.#credentials = BSON.deserialize(GPG.decrypt(Buffer.from(encrypted, 'base64'), this.#pw))
      }
      catch (error) { 
         this.#credentials = {}
         this.close()
         throw `Failed to decrypt: ${path.split('/').pop()}\nError: ${error.message}`
      }
   }


   #initialize () {
      const path = this.#path
      if (fs.existsSync(`${path}.lock`)) {
         throw fs.readFileSync(`${path}.lock`)
         
         //!`Credentials store (${path.split('/').pop()}) is currently locked.\nIf no CredentialsManager running, safe to delete associated '.lock' file.`
      }
      else {
         fs.writeFileSync(`${path}.lock`, this.#lockMessage, {flags: 'w+'})
         this.hasLock = true
      }
      
      const exists = fs.existsSync(path)
      if (!exists) this.#createStore()   
      else this.#reloadStore()


      this.#index = new Index(Object.keys(this.#credentials))
      return true
   }

   #encryptStore () {
      let { owner } = this.#store
      this.#store.encrypted = GPG.encrypt(BSON.serialize(this.#credentials), owner, this.#pw).toString('base64')
      fs.writeFileSync(this.#path, JSON.stringify(this.#store), { flags: 'w+' })
   }




   #abort (message) {
      
   }


   has (id) {
      return Boolean(Object.keys(this.#credentials).includes(id))
   }

   set (id, data) {
      if (!this.#credentials) return "Locked."

      this.#credentials[id] = (data instanceof Object)
         ? JSON.stringify(data)
         : data

      this.#index = new Index(Object.keys(this.#credentials))
      this.#store.modified = Date.now()
      this.#encryptStore()
   }

   get (id) {
      if (!this.#credentials) return "Locked."
      try {
         return JSON.parse(this.#credentials[id])
      }
      catch (error) {
         return this.#credentials[id]
      }
   }

   list () {
      if (!this.#credentials) return "Locked."
      return Object.keys(this.#credentials)
   }

   search (query) {
      if (!this.#credentials) return "Locked."
      return this.#index.search(query).map(credential => credential.item)
   }

   findOne (query) {
      if (!this.#credentials) return "Locked."
      return this.#index.search(query)[0]?.item || null
   }

   close () {
      let file = this.#path.split('/').pop()
      if (!fs.existsSync(this.#path)) return true
      if (!this.#credentials) throw `Does not have lock for ${file}.`

      this.#credentials = null
      this.#store = null
      this.#index = null
      this.#pw = null

      fs.unlinkSync(`${this.#path}.lock`)
      return true
   }

   open (pw) {
      if (this.#credentials) return "Unlocked."
      //if (!pw) return "Provide password to unlock."
      //? How does GPG respond to non-password-protected keys?

      if (!GPG.authenticate(this.owner, pw)) Log("Failed to authenticate.")
      else {
         Log('Unlocking Credentials Store.')
         this.#initialize()
      }
   }
}


module.exports = CredentialsManager

/* Usage:
```js
let credentials = new CredentialsManager(path, { owner, pw })

// . . . 

let { pid } = process.on('SIGTERM', test.exit)
process.kill(pid, 'SIGTERM', () => {
   credentials.close()
   credentials = null;
   delete credentials
}
```*/



/* Exit.on('exit', async () => {
   Log('\nGracefully exiting')
   this.close()
}) */ //! Cannot be triggered from here. Caller must .close()