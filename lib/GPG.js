const sh = require('child_process')
const fs = require('fs')
const Log = console.log


requireGPG() //* Requires GnuPG, gpg available on PATH.

module.exports = {
   requireGPG,

   encrypt,
   decrypt,
   //passwordEncrypt, (symmetric)
   //passwordDecrypt

   importKeys,
   generateKeys,
   exportPublicKey,
   exportSecretKey,

   deleteKeys,
   deleteSecretKey,
   deletePublicKey,

   keysExist,
   listKeys,
   searchKeys,
   identifyKeys,
   authenticate,
   isAuthenticated,

   setExpiration,
   trustIdentity,
   trustMarginally,
   trustFully,
   trustUltimately,
   isUltimatelyTrusted,
   isTrusted
}



function requireGPG () {
   try {
      sh.execSync('gpg --version', { stdio: 'ignore' })
   }
   catch (err) {
      throw "The 'gpg' module requires GnuPG to be installed on your system."
   }
}


/* function generateKeys (identity={}) {
   if (!identity.id)
} */

function generateKeys (id, pw='') { //, opts={}) { 
   if (keysExist(id)) throw `'${id}' already exists.`
   
   sh.execSync(`gpg --batch --passphrase-fd 0 --quick-gen-key ${id} > /dev/null 2>&1`, {
      input: pw,
      stdio: 'ignore' //! stdio: 'ignore' Ignores Input.
   })

   return keysExist(id)
}


function encrypt (data, id) {  // pw=null) 
   if (!data) throw "GPG requires data." 
   if (!id) throw "GPG requires recipient identity."
   if (!keysExist(id)) throw `Unknown recipient: ${id}`

   /* //? Is supplying password ever required for encryption?
   const pass = () => `--pinentry-mode loopback --passphrase=${pw} `
   let command = (pw)
      ? `gpg ${pass()}-esr ${recipient} -o-`
      : `gpg --encrypt -sr ${recipient} -o-` */

   const command = `gpg --encrypt --sign --recipient ${id} -o-`
   return sh.execSync(command, { input: data })
}


function decrypt (data, pw=null) {
   if (!data) throw "GPG requires data." 

   const pass = () => `--pinentry-mode loopback --passphrase=${pw} `
   let command = (pw)
      ? `gpg ${pass()}-d -o- 2>/dev/null`
      : `gpg --decrypt -o- 2>/dev/null`
   try {
      return sh.execSync(command, { input: data })
   }
   catch (error) {
      return [false, error.message]
   }
}


/* function passwordEncrypt (data, pw) {
   gpg -o doc.gpg --encrypt --recipient blake@cyb.org doc
}
function passwordDecrypt (data, pw) {
   gpg --output doc --decrypt doc.gpg
} */


function keysExist (identifier) {
   if (!identifier) throw 'GPG.keysExist() was not provided an identifier.'

   let keys = searchKeys(identifier)
   if (!keys.length) return false

   return keys.some(key => [key.id, key.fingerprint].includes(identifier))
}


function listKeys () {
   return searchKeys('')
}


function searchKeys (identifier=null) {
   const command = `gpg --list-key ${identifier||''} | tr -s ' ' `
   const stdio = [ null, null, 'ignore' ]

   let found = null
   try {
      found = sh.execSync(command, { stdio }).toString()
   } catch (e) {}

   const trust = getTrustLevels()

   return (!found)
      ? []
      : found.split('\n\n').slice(0,-1)
         .map(extractKeyDetails)
         .map(keys => {
            keys.trust = trust[keys.fingerprint]
            return keys
         })
}


function identifyKeys (identifier) {
   if (!identifier) throw "No identifier provided."
   let keys = searchKeys(identifier)
   return keys.find(key => [key.id, key.fingerprint].includes(identifier)) || null
}


//? Make this Asynchronous?
function authenticate (identifier, pw) {
   requireKeysExist(identifier)
   let { fingerprint } = identifyKeys(identifier)

   const env = {
      ...process.env,
      GPG_AGENT_INFO: ""
   }

   try {
      sh.execSync(`gpg --batch --pinentry-mode=loopback --passphrase-fd=0 --armor --export-secret-key ${fingerprint}`, { 
         input: pw,
         stdio:['pipe', 'ignore', 'ignore'],
         env
      })
      return true
   }
   catch (error) {
      return false
   }
}

function isAuthenticated (identifier) {
   requireKeysExist(identifier)

   try {
      sh.execSync(`gpg -o /dev/null -a --export-secret-key ${identifier}`, {stdio: 'ignore' })
      return true
   }
   catch (error) {
      return false
   }
}


function importKeys (key) {
   try {
      sh.execSync(`gpg --import >/dev/null 2>&1`, { input: key })
      return true
   }
   catch (error) {
      Log(error.message)
      return false
   }
}


function deleteSecretKey (identifier, pw=null) {
   let { fingerprint } = identifyKeys(identifier)
   const args = ['--expert', '--batch', '--command-fd=0', '--delete-secret-key', '--yes', fingerprint]
   const input = Buffer.from(`y\ny\n`)
   let response = sh.spawnSync('gpg', args, { input })
   if (response.status === 0) return true
   else {
      Log(response.stderr.toString().split('\n').filter(line => line.startsWith('gpg: ')).join('\n'))
      return false
   }
}

function deletePublicKey (id, pw=null) {
   const args = ['--command-fd=0', '--delete-key', '--yes', identifier]
   const input = Buffer.from(`y\ny\n`)
   let response = sh.spawnSync('gpg', args, { input })
   if (response.status === 0) return true
   else {
      Log(response.stderr.toString().split('\n').filter(line => line.startsWith('gpg: ')).join('\n'))
      return false
   }
}

function deleteKeys (id, pw=null) {
   if (!deleteSecretKey(id, pw)) return false
   deletePublicKey(id, pw)
}


function setExpiration (id, expires, options={}) {
   //?if (!identity.keys().includes('id') && !identity.keys().includes('fingerprint') ) throw "No identity provided"

   let { fingerprint } = GPG.identifyKeys(id)
   if (!expires) expires = 'never'

   let { pw } = options
   let args = ['--batch', '--quick-set-expire', fingerprint, expires]
   if (pw) args.unshift(...['--pinentry-mode=loopback', `--passphrase=${pw}`])

   Log('setExpiration(), args:')
   console.dir(args)

   let res = sh.spawnSync('gpg', args, { stdio: 'ignore' })
   return Boolean(res.status === 0)

   if (status === 0) return true
   Log(statu)

}


function trustIdentity (id, level=null) {
   //? if (!authenticate(id)) throw `Not authenticated as ${id}`
   if (!keysExist(id)) throw "Keys"

   const fingerprint = identifyKeys(id).fingerprint
   let trustLevel = 1
   switch (level) {
      case false:
         trustLevel = 2; break;
      case 'marginal':
         trustLevel = 3; break;
      case 'full':
         trustLevel = 4; break;
      case 'ultimate':
         trustLevel = 5; break;
   }



   //* This works but is not silent:
   //! sh.execSync(`gpg --command-fd=0 --expert --edit-key ${fingerprint} trust quit;`, { input })

   //* Silence output:
   //! let response = sh.spawnSync('gpg', ['--command-fd=0', '--expert', '--edit-key', fingerprint, 'trust', 'quit'], { input, detached: true })
   //?return Boolean(response.status === 0)

   const input = Buffer.from(`${trustLevel}\ny\n`)

   let response = sh.spawnSync('gpg', ['--batch', '--command-fd=0', '--expert', '--edit-key', fingerprint, 'trust', 'quit'], { input })
   if (response.status === 0) return true
   else {
      Log(response.stderr.toString().split('\n').filter(line => line.startsWith('gpg: ')).join('\n'))
      return false
   }
}


function getTrustLevels () {
   const getTrustLevel = (level) => {
      switch (Number.parseInt(level)) {
         case 2:
            return null
         case 3:
            return false
         case 4:
            return 'marginal'
         case 5:
            return 'full'
         case 6:
            return 'ultimate'
      }
   }

   let fingerprints = {}
   sh.execSync('gpg --export-ownertrust')
      .toString().trim().split('\n')
      .filter(line => !line.startsWith('#'))
      .forEach(entry => {
         let [fingerprint, level] = entry.split(':').slice(0,-1)
         fingerprints[fingerprint] = getTrustLevel(level)
      })
   return fingerprints
}


function trustUltimately (id) { //? need passphrase
   return trustIdentity(id, 'ultimate')
}


function trustFully (id) { //? need passphrase
   return trustIdentity(id, 'full')
}


function trustMarginally (id) { //? need passphrase
   return trustIdentity(id, 'marginal')
}


function isTrusted (identifier) {
   requireKeysExist(identifier)

   let keys = searchKeys(identifier)
   return Boolean(keys.find(key => [key.id, key.fingerprint].includes(identifier))?.trust)
}


function isUltimatelyTrusted (identifier) {
   requireKeysExist(identifier)

   let keys = searchKeys(identifier)
   return Boolean(keys.find(key => [key.id, key.fingerprint].includes(identifier))?.trust === 'ultimate')
}


function extractKeyDetails (gpgOutput) { // Takes output from `gpg --list-key` for a single id.
   const interpretFlags = (flag) => {
   // Certify, Sign, Authenticate, Encrypt:
      return Array.from(flag).map(letter => {
         switch (letter) {
            case 'C': return 'Certify'
            case 'S': return 'Sign'
            case 'A': return 'Authenticate'
            case 'E': return 'Encrypt'
         }

         if (letter === 'C') flags.push('Certify')
         if (letter === 'S') flags.push('Sign')
         if (letter === 'A') flags.push('Authenticate')
         if (letter === 'E') flags.push('Encrypt')
      })
   }

   const keyDetails = (line) => {
      if (!line) return null
      let details = line.split(' ').slice(1)
      
      let [ type, created, flags ] = details
      let expires = details.some(entry => entry.includes('expires:'))
         ? details.slice(-1)[0].slice(0,-1)
         : null

      flags = interpretFlags(Array.from(flags).slice(1,-1))
      return { type, flags, created, expires }
   }

   let lines = gpgOutput.split('\n')
      .filter(line => !line.startsWith("gpg: ")) //? Strip header (GPG message prefixed with: "gpg: ")


   //! the 'trust' that is listed on --gpg-list-key is insufficient.
   //* Additionally, call: --gpg --export-ownertrust && Parse.
   let [ trust, id ] = lines.find(line => line.startsWith('uid')).split(/\[|\]/).map(e => e.trim()).slice(1)
   let fingerprint = lines.find(line => line.startsWith(' ')).trim()
   let pub = lines.find(line => line.startsWith('pub')) || null
   let sub = lines.find(line => line.startsWith('sub')) || null

   return {
      id,
      trust,
      fingerprint,
      pub: keyDetails(pub),
      sub: keyDetails(sub)
   }
}




function exportPublicKey (id, pw=null) {
   requireKeysExist(id)

   const command = `gpg --batch --passphrase-fd 0 --armor --export ${id} 2> /dev/null`
   const input = (typeof pw === 'string') ? pw : null

   try {
      return sh.execSync(command, { input })
   }
   catch (error) {
      Log(error.message)
      return null
   }
}


function exportSecretKey (id, pw=null) {
   requireKeysExist(id)
   //!if (!authenticate(id, pw)) throw `Not authorized to export Private Key for ${id}`

   try { //? Is prompting. Can we fix that?
      return sh.execSync(`gpg --batch --passphrase-fd 0 --armor --export-secret-key ${id} 2> /dev/null`)
   }
   catch (error) {
      Log(error.message)
      return null
   }
}



function requireKeysExist (id) {
   if (!keysExist(id)) throw `GPG keys do not exist for ${id}`
}