const os = require('os')
const fs = require('fs')
const sh = require('child_process')
const Log = console.log
const Path = require('path')
const Sudo = require('sudo-prompt')
const Crypto = require('crypto')
const Inquire = require('inquirer')

const Debug = require('debug')
const debug = Debug('security')



const HOME = process.env.HOME
const USER = HOME.split('/').pop()

//? Tested Ubuntu only:
const SYSTEM = {
   CA: {
      name: 'Webkit_API',
      install: '/usr/local/share/ca-certificates',
      installed: '/etc/ssl/certs'
   }
}

const WEBKIT = {
   config: Path.join(HOME, '.config/webkit')
}

const GPG = require('./GPG')
const OpenSSL = require('./OpenSSL')
const CredentialsManager = require('./CredentialsManager')


const temporaryDirectory = () => fs.mkdtempSync(fs.realpathSync(os.tmpdir()) + Path.sep)
const randomFilename = () => Crypto.randomBytes(3).toString('base64').replace('/', '_')



class CertificateAuthority {  
   #rootCert
   #certificates
   constructor (name=SYSTEM.CA.name, domain='localhost') {
      this.name = name || SYSTEM.CA.name
      this.domain = domain
      this.#rootCert = {
         crt: Path.join(SYSTEM.CA.install, `${this.name}.crt`),
         pem: Path.join(SYSTEM.CA.installed, `${this.name}.pem`)
      }
      debug(this)
   }


   async establish (authority, options={}) {

      //* Pick up ID from owner of security.json

      let { id, pw, sudo=null } = authority //Master Publisher
      let { path=null, overwrite=false } = options
      
      path = path || Path.join(WEBKIT.config, 'security.json')

      if (!GPG.isUltimatelyTrusted(id)) {
         //* Prompt: trust Ultimately?
         throw `${id} is not ultimately trusted in GPG keyring.`
      }

      this.#certificates = new CredentialsManager(path, { owner: id, pw })
      if (!this.#certificates.has('CertificateAuthority')) this.#generate()

      debug(this)

      let verified = fs.existsSync(this.#rootCert.pem)
         ? await this.#verify({ sudo, overwrite })
         : null
      if (verified) return this.name
      else return (await this.#install({ sudo })) ? this.name : false
   }

   close () {
      if (!fs.existsSync(Path.join(WEBKIT.config, 'security.json.lock'))) return true
      if (!this.#certificates) throw `Does not have lock.`
      else return this.#certificates.close()
   }


   #generate () {
      const key = OpenSSL.PrivateKey()
      const CA = {
         key,
         cert: (this.domain === 'localhost')
            ? OpenSSL.SelfSignedCert(key)
            : (() => { throw "Let's Encrypt not implimented yet." })()
      }

      this.#certificates.set('CertificateAuthority', {
         key: CA.key.toString(),
         cert: CA.cert.toString()
      })
   }


   async #verify ({ sudo=null, overwrite=false }={}) {
      if (!fs.existsSync(this.#rootCert.pem)) return false

      //! Test sudo password.

      let rootCert = (sudo)
         ? sh.execSync(`sudo -S cat ${this.#rootCert.pem}`, {input: sudo}).toString()
         : await sudoExec(`cat ${this.#rootCert.pem}`)
      if (rootCert === this.#certificates.get('CertificateAuthority').cert) return true

      await this.#uninstall({ sudo, overwrite })
      if (fs.existsSync(rootCert)) {
         throw `Failed to uninstall Certificate Authority: '${this.name}'.`
      }
      return false   
   }


   async #install ({ sudo=null }={}) {
      let tempDir = temporaryDirectory()
      let tempFile = Path.join(tempDir, randomFilename())

      try {
         fs.writeFileSync(tempFile, this.#certificates.get('CertificateAuthority').cert, { flags: 'w+' })

         if (sudo) {
            sh.execSync(`sudo -S mv ${tempFile} ${this.#rootCert.crt}`, {input: sudo})
            sh.execSync(`sudo -S update-ca-certificates`, {input: sudo})
         }
         else await sudoExec(`mv ${tempFile} ${this.#rootCert.crt} && update-ca-certificates`)
      }

      finally {
         fs.rmSync(tempDir, {recursive: true});
      }

      return Boolean(fs.existsSync(Path.join(SYSTEM.CA.installed, `${SYSTEM.CA.name}.pem`)))
   }


   async #uninstall ({ sudo=null, overwrite=false }={}) {
      if (!overwrite) {
         Log(`The current Webkit configuration (~/.config/webkit/security.json) will overwrite the installed 'Webkit.pem' root certificate installed on this system.`)
         Log(`'${this.#rootCert.pem}'`)

         let { confirm } = await Inquire.prompt([{
            type: 'confirm',
            name: 'confirm',
            message: `Overwrite trusted ${this.name} certificate?`
         }])

         if (!confirm) {
            Log(`Not sure how to procede without overwriting.`)
            throw `Manually delete '${this.#rootCert.pem}' to continue.`
         }
      }

      //* Uninstall root certificate:
      if (sudo) sh.execSync(`sudo -S rm ${this.#rootCert.crt}`, {input: sudo}).toString()
      else await sudoExec(`rm ${this.#rootCert.crt} && update-ca-certificates -f`)
   }


   issueClient (id) {
      id = id || USER
      const client = { id }
      
      if (this.#certificates.has(`clients/${id}`)) throw `Client certificate already exists: ${id}`

      const CA = this.#certificates.get('CertificateAuthority')
      const serial = 1 + this.#certificates.search('clients/').length
      
      const service = this.name
      try {
         client.key = OpenSSL.PrivateKey()
         client.CSR = OpenSSL.SigningRequest(service, client)
         client.cert = OpenSSL.SignedClientCert(CA, client, serial)
         client.p12 = OpenSSL.packageCredentials(client)   
      }
      catch (error) {
         debug(error.message)
         return false
      }

      this.#certificates.set(`clients/${id}`, {
         key: client.key.toString(),
         CSR: client.CSR.toString(),
         cert: client.cert.toString(),
         p12: client.p12.toString()
      })


      try {
         //* Do we need to write the files?
         const clients = Path.join(WEBKIT.config, 'clients')
         if (!fs.existsSync(clients)) fs.mkdirSync(clients)
         fs.writeFileSync(Path.join(clients, `${id}.cert`), client.cert)
         fs.writeFileSync(Path.join(clients, `${id}.key`), client.key)
         fs.writeFileSync(Path.join(clients, `${id}.p12`), client.key)
         //? Write client credentials (key & cert) to ~/.config/webkit/clients
         return true
      }
      catch (error) {
         debug(error.message)
         return false
      }
   }

   listClients () {
      return this.#certificates.search('clients/')
   }

   clientCredentials (id) {
      if (!this.#certificates.has(`clients/${id}`)) throw `Credentials have not been issued for ${id}`
      return this.#certificates.get(`clients/${id}`)
   }

   renewClientCertificate (id) {
      throw "Not implimented yet."

      Log('[Renewing Server/Client Certificates](https://www.golinuxcloud.com/renew-ssl-tls-server-certificate-openssl)')
   }
}

module.exports = CertificateAuthority

  
//? Install & Uninstall Root Certificate with Sudo:
//*sudoExec(`cp ${certPath} /usr/local/share/ca-certificates && update-ca-certificates`)
//!sudoExec(`rm /usr/local/share/ca-certificates/Webkit_API.crt && update-ca-certificates -f`)

function sudoExec (command, options={}) {
   return new Promise((resolve, reject) => {
      Sudo.exec(command, options,
      (error, stdout, stderr) => {
        if (error) reject(stderr)
        else resolve(stdout)
      })
   })
}
