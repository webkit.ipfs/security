const os = require('os')
const fs = require('fs')
const sh = require('child_process')
const Log = console.log
const Path = require('path')
const Crypto = require('crypto')

const Debug = require('debug')
const debug = Debug('security')


const temporaryDirectory = () => fs.mkdtempSync(fs.realpathSync(os.tmpdir()) + Path.sep)
const randomFilename = () => Crypto.randomBytes(3).toString('base64').replace('/', '_')


//![issue](Set to never expire?)
const expiry = 3650 //* Expires in 10 years

const OpenSSL = {
   PrivateKey: () => sh.execSync(`openssl genrsa 4096 2> /dev/null`),

   PublicKey: (privateKey) => sh.execSync(`openssl rsa -in - -pubout 2> /dev/null`, { input: privateKey }),

   SelfSignedCert (privateKey, config={}) {
      let { domain='localhost', alt='webkit.service' } = config
      subj=`/CN=${domain}/O=${alt}`
      return sh.execSync(`openssl req -new -x509 -key - -days ${expiry} -subj ${subj}`, { input: privateKey })
   },

   LetsEncryptSignedCert: () => "Let's Encrypt not implimented yet.",

   SigningRequest (service, { id, key }) {
      if (!service) throw "OpenSSL.SigningRequest() requires service."
      if (!id) throw "OpenSSL.SigningRequest() requires client.uid"
      if (!key) throw "OpenSSL.SigningRequest() requires client.key"
      return sh.execSync(`openssl req -new -key - -nodes -subj "/CN=${id}@${service}"`, { input: key })
   },

   SignedClientCert (CA, client, serial) {
      if (!Number.isInteger(Number.parseInt(serial))) throw "OpenSSL.SignedClientCert() Requires client serial number."

      const dir = temporaryDirectory().toString()
      const files = {
         'CA.key': {
            path: Path.join(dir, randomFilename()),
            data: CA.key
         },
         'CA.cert': {
            path: Path.join(dir, randomFilename()),
            data: CA.cert
         },
         'client.key': {
            path: Path.join(dir, randomFilename()),
            data: client.key
         },
         'CSR': {
            path: Path.join(dir, randomFilename()),
            data: client.CSR
         }
      }
   
      client.serial = `${serial}`.padStart(4, "0")
   
      let certificate = null
      try {
         Object.keys(files).forEach(file => {
            let { path, data } = files[file]
            fs.writeFileSync(path, data, { flags: 'w+' })
         })
   
         certificate = sh.execSync(`openssl x509 -req ${[
            `-days ${expiry}`,
            `-in ${files['CSR'].path}`,
            `-CA ${files['CA.cert'].path}`,
            `-CAkey ${files['CA.key'].path}`,
            `-set_serial ${client.serial}`
         ].join(' ')} 2> /dev/null`) //? reference: https://gist.github.com/mtigas/952344
         //!console.log(certificate)
      }
      finally {
         //! fs.readdirSync(dir)
         fs.rmSync(dir, {recursive: true});
         //? debug(`'${dir}' Deleted: ${Boolean(!fs.existsSync(dir))}`)
      }
      return certificate
   },

   packageCredentials (client) {
      const dir = temporaryDirectory().toString()
      const files = {
         'client.key': {
            path: Path.join(dir, randomFilename()),
            data: client.key
         },
         'client.cert': {
            path: Path.join(dir, randomFilename()),
            data: client.cert
         }
      }
   
      let p12 = null
      try {
         Object.keys(files).forEach(file => {
            let { path, data } = files[file]
            fs.writeFileSync(path, data, { flags: 'w+' })
         })
         let key = files['client.key'].path
         let cert = files['client.cert'].path
         p12 = sh.execSync(`openssl pkcs12 -export -clcerts -in ${cert} -inkey ${key} -password pass:`) //  2> /dev/null
      }
      finally {
         fs.rmSync(dir, {recursive: true});
      }
      return p12
   },

   // describeP12 (p12) => sh.execSync(`openssl pkcs12 -info -nodes -in -`, { input: p12 })

   isExpiring (certificate) {
      const seconds = 86400
      try {
         sh.execSync(`openssl x509 -checkend ${seconds} -noout -in - 2> /dev/null`, { input: certificate })
         return true
      }
      catch (error) {
         return false
      }
   }

   //? setNeverExpire . . . 
}

module.exports = OpenSSL




/* module.exports = {
   PrivateKey,
   PublicKey,
   SelfSignedCert,
   SigningRequest,
   SignedClientCert,
   packageCredentials,
   //describeP12,
   isExpiring
} */


/* 
function PrivateKey () {
   return sh.execSync(`openssl genrsa 4096 2> /dev/null`)
}


function PublicKey (privateKey) {
   return sh.execSync(`openssl rsa -in - -pubout 2> /dev/null`, { input: privateKey })
}


function SelfSignedCert (privateKey, config={}) {
   let { domain='localhost', alt='webkit.service' } = config
   subj=`/CN=${domain}/O=${alt}`
   return sh.execSync(`openssl req -new -x509 -key - -days ${expiry} -subj ${subj}`, { input: privateKey })
}


function LetsEncryptSignedCert () {
   return "Let's Encrypt not implimented yet."
}


function SigningRequest (client) {
   let { uid, key } = client
   return sh.execSync(`openssl req -new -key - -nodes -subj "/CN=${uid}@webkit.service"`, { input: key })
}


function SignedClientCert (CA, client, serial) { //! Needs client serial number.
   const dir = temporaryDirectory().toString()
   const files = {
      'CA.key': {
         path: Path.join(dir, randomFilename()),
         data: CA.key
      },
      'CA.cert': {
         path: Path.join(dir, randomFilename()),
         data: CA.cert
      },
      'client.key': {
         path: Path.join(dir, randomFilename()),
         data: client.key
      },
      'CSR': {
         path: Path.join(dir, randomFilename()),
         data: client.CSR
      }
   }

   client.serial = serial || '01' //* Count clients. Issue unique serial.

   let certificate = null
   try {
      Object.keys(files).forEach(file => {
         let { path, data } = files[file]
         fs.writeFileSync(path, data, { flags: 'w+' })
      })

      certificate = sh.execSync(`openssl x509 -req ${[
         `-days ${expiry}`,
         `-in ${files['CSR'].path}`,
         `-CA ${files['CA.cert'].path}`,
         `-CAkey ${files['CA.key'].path}`,
         `-set_serial ${client.serial}`
      ].join(' ')} 2> /dev/null`) //? reference: https://gist.github.com/mtigas/952344
      //!console.log(certificate)
   }
   finally {
      //! fs.readdirSync(dir)
      fs.rmSync(dir, {recursive: true});
      //? debug(`'${dir}' Deleted: ${Boolean(!fs.existsSync(dir))}`)
   }

   return certificate
}


function packageCredentials (client) {
   const dir = temporaryDirectory().toString()
   const files = {
      'client.key': {
         path: Path.join(dir, randomFilename()),
         data: client.key
      },
      'client.cert': {
         path: Path.join(dir, randomFilename()),
         data: client.cert
      }
   }

   let p12 = null
   try {
      Object.keys(files).forEach(file => {
         let { path, data } = files[file]
         fs.writeFileSync(path, data, { flags: 'w+' })
      })
      let key = files['client.key'].path
      let cert = files['client.cert'].path
      p12 = sh.execSync(`openssl pkcs12 -export -clcerts -in ${cert} -inkey ${key} 2> /dev/null`)
   }
   finally {
      fs.rmSync(dir, {recursive: true});
   }
   return p12
}


//function describeP12 (p12) {
//   sh.execSync(`openssl pkcs12 -info -nodes -in -`, { input: p12 })
//}

function isExpiring (certificate) {
   const seconds = 86400
   try {
      sh.execSync(`openssl x509 -checkend ${seconds} -noout -in - 2> /dev/null`, { input: certificate })
      return true
   }
   catch (error) {
      return false
   }
}


module.exports = {
   PrivateKey,
   PublicKey,
   SelfSignedCert,
   SigningRequest,
   SignedClientCert,
   packageCredentials,
   //describeP12,
   isExpiring
}

*/
