const log = console.log

const fs = require('fs')
const Path = require('path')

const https = require('https')
const Request = require('axios')

const HOME = process.env.HOME

const WEBKIT = {}
WEBKIT.certs = Path.join(HOME, '.config/webkit/.certs')
WEBKIT.port = 9500

class SecureRequest {
   constructor (config={}) {
      this.agent = null
      this.ready = false
      this.domain = null
      this.configure(config)
   }

   configure (config={}) {
      let port = config.port || WEBKIT.port
      let domain = config.domain || 'localhost'
      this.domain = `https://${domain}:${port}`

      let clientCert = (config.clientCert)
         ? config.clientCert
         : Path.join(WEBKIT.certs, 'client.cert.pem') //?'client.cert')
      let clientKey = (config.clientKey)
         ? config.clientKey
         : Path.join(WEBKIT.certs, 'client.key.pem')
      let authority = (config.ca)
         ? config.ca
         : Path.join(WEBKIT.certs, 'service.cert')

      try {
         this.agent = new https.Agent({
            port,
            cert: fs.readFileSync(clientCert),
            key: fs.readFileSync(clientKey),
            ca: fs.readFileSync(authority),
            servername: domain
         })
         this.ready = true
         return true
      }
      catch (error) {
         return error.message
      }
   }

   async get (route='/') {
      const endpoint = Path.join(this.domain, route)
      const httpsAgent = this.agent
      
      try {
         let response = await Request.get(endpoint, { httpsAgent }) 
         return response
      }
      catch (error) {
         return error.message
      }
   }

   async post (route='/', data=null) {
      const endpoint = Path.join(this.domain, route)
      const httpsAgent = this.agent
      return (data)
         ? await Request.post(endpoint, { httpsAgent, data })
         : await Request.post(endpoint, { httpsAgent })
   }
}

module.exports = new SecureRequest()

/*
const SecureRequest = require('./SecureRequest')

SecureRequest.get('/route')
SecureRequest.post('/route', data)
*/

//? Optionally, configure:
/*
{ 
   domain: 'localhost',
   port: 9500, 
   clientCert,
   clientKey,

 }
SecureRequest.configure({
   
})
const SecureRequest = new require('.')() //? 
*/
