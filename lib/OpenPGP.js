const sh = require('child_process')
const log = console.log
const Path = require('path')

const OpenPGP = require('openpgp')

const HOME = process.env.HOME
const WEBKIT = {
   config: Path.join(HOME, `.config/webkit`)
}

module.exports = {

   async generateKeys (trustees, passphrase=null) {
      if (!Array.isArray(trustees)) trustees = [trustees]

      if (trustees.find((trustee, t) => {
         return Boolean(!Object.keys(trustee).includes('email') || !Object.keys(trustee).includes('name'))
      })) {
         throw "Requires: { email, name }"
      }

      return await OpenPGP.generateKey({
         userIDs: trustees,
         passphrase,
         format: 'binary'
      })

   },

   generateToken () {
      let { id, nid } = this.publisher

      let token = Path.join(WEBKIT.config, `./publishers/${id}/.session`)
      //? Option to write token as 'obect' rather than 'armored'
      //  -> save to .session.json with PID of running process.
      //! not needed because: curl http://localhost:$port/logout

      const { privateKey } = await OpenPGP.generateKey({
         userIDs: [{ name: id, email: nid }],
         //?passphrase: publisher.pw, // protects the private key
         format: 'armored'
     });
   
     fs.writeFileSync(token, privateKey)
     return privateKey
   },


   /* encrypt: (string, nid, pw=null) => {
      if (!nid) throw "GPG requires recipient.\nFor passphrase-only encryption, use: `PGP.encrypt(data, { passwords:['secret'] })`"
      const pass = () => `--pinentry-mode loopback --passphrase=${pw} `
      let command = (pw)
         ? `gpg ${pass()}-esr ${nid} -o-`
         : `gpg --encrypt -sr ${nid} -o-`

      return sh.execSync(command, { input: string }).toString('base64')
   }, */

   /* decrypt: (data, recipient={}) => {
      let { pw=null } = recipient
      const pass = () => `--pinentry-mode loopback --passphrase=${pw} `
      let command = (pw)
         ? `gpg ${pass()}-d -o- 2>/dev/null`
         : `gpg --decrypt -o- 2>/dev/null`

      return sh.execSync(command, { input: Buffer.from(data, 'base64') }).toString()
   } */

   //? encryptArmor
   //? decryptArmor //? detectArmor?
}

module.exports = GPG

/*
const GPG = require('.')
let publisher = { nid: 'yarrow' }

let encrypted = GPG.encrypt('SECRET MESSAGE', publisher)
GPG.decrypt(encrypted)
*/
