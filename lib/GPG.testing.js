const fs = require('fs')
const Log = console.log
const Exit = require('node-graceful')

const GPG = require('./GPG')
const BSON = require('bson')
const Index = require('fuse.js')
const { exit } = require('process')


const Owner = (path) => JSON.parse(fs.readFileSync(path).toString()).owner

class CredentialsManager {
   #path
   #store
   #credentials
   #index

   #pw
   #lockMessage = `Locked by Webkit.CredentialsManager, pid: ${process.pid}`

   constructor (path, { owner=null, pw=null }={}) {
      if (!path) throw "CredentialsManager constructor requires the path to the credentials store."
      this.owner = fs.existsSync(path)
         ? Owner(path)
         : owner || USER
      this.#pw = pw
      this.#path = path
      this.#store = null
      this.#index = null
      this.#loadStore()
   }


   has (id) {
      return Boolean(Object.keys(this.#credentials).includes(id))
   }

   set (id, data) {
      if (!this.#credentials) return "Locked."

      this.#credentials[id] = (data instanceof Object)
         ? JSON.stringify(data)
         : data

      this.#index = new Index(Object.keys(this.#credentials))
      this.#store.modified = Date.now()
      this.#encryptStore()
   }

   get (id) {
      if (!this.#credentials) return "Locked."
      try {
         return JSON.parse(this.#credentials[id])
      }
      catch (error) {
         return this.#credentials[id]
      }
   }

   list () {
      if (!this.#credentials) return "Locked."
      return Object.keys(this.#credentials)
   }

   search (query) {
      if (!this.#credentials) return "Locked."
      return this.#index.search(query).map(credential => credential.item)
   }

   findOne (query) {
      if (!this.#credentials) return "Locked."
      return this.#index.search(query)[0]?.item || null
   }

   close () {
      let file = this.#path.split('/').pop()
      if (!fs.existsSync(this.#path)) return true
      if (!this.#credentials) throw `Does not have lock for ${file}.`

      this.#credentials = null
      this.#store = null
      this.#index = null
      this.#pw = null

      fs.unlinkSync(`${this.#path}.lock`)
      return true
   }

   async open (pw) {
      if (this.#credentials) return "Unlocked."
      //if (!pw) return "Provide password to unlock."
      //? How does GPG respond to non-password-protected keys?

      let { owner } = this
      let path = this.#path
      //? Make this Async??
      if (!GPG.authenticate(owner, pw)) Log("Failed to authenticate.")
      else {
         Log('Unlocking Credentials Store.')
         this.#loadStore()
      }
   }


   #loadStore () {
      const path = this.#path

      const exit = () => {
         this.#credentials = {}
         this.close()
      }

      const Locked = fs.existsSync(`${path}.lock`)
      if (Locked) throw `Credentials store (${path.split('/').pop()}) is currently locked.\nIf no CredentialsManager running, safe to delete associated '.lock' file.`
      fs.writeFileSync(`${path}.lock`, this.#lockMessage, {flags: 'w+'})
            
      Exit.on('exit', async () => {
         Log('\nGracefully exiting')
         this.close()
      })

      //? Create new store:
      if (!fs.existsSync(path)) {
         let created = Date.now()

         this.#credentials = {}
         this.#store = {
            owner: this.owner,
            created,
            modified: null,
            encrypted: null
         }
         this.#encryptStore()

      } else { //? Load existing store:
         try { // Read & Parse `.json` file:
            this.#store = JSON.parse(fs.readFileSync(path))
         }
         catch (error) { 
            exit()
            throw "Not a credential store." 
         }

         try { // Decrypt 'encrypted.data', index credentials:
            let { encrypted } = this.#store
            this.#credentials = BSON.deserialize(GPG.decrypt(Buffer.from(encrypted, 'base64'), this.#pw))
         }
         catch (error) { 
            exit()
            throw `Failed to decrypt: ${path.split('/').pop()}\nError: ${error.message}`
         }
      }

      this.#index = new Index(Object.keys(this.#credentials))
      return true
   }

   #encryptStore () {
      let { owner } = this.#store
      this.#store.encrypted = GPG.encrypt(BSON.serialize(this.#credentials), owner, this.#pw).toString('base64')
      fs.writeFileSync(this.#path, JSON.stringify(this.#store), { flags: 'w+' })
   }


}


module.exports = CredentialsManager

/* Usage:
```js
let credentials = new CredentialsManager(path, { owner, pw })

// . . . 

let { pid } = process.on('SIGTERM', test.exit)
process.kill(pid, 'SIGTERM', () => {
   credentials.close()
   credentials = null;
   delete credentials
}
```*/
