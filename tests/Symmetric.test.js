const { GPG, OpenPGP } = require('@webkit.ipfs/security')

const sh = require('child_process')

test();
async function test () {

    let alice = {
        email: 'alice@my.org',
        publicKey: GPG.PublicKey('alice@my.org').toString()
    }

    console.log("Alice's public key:")
    console.log(alice.publicKey)

    let bob = {
        email: 'bob@my.org',
        ...(await PGP.generateKey({
            userIDs: [{name:'Bob',email:'bob@my.org'}]
        }))
    }

    console.log("Bob's public key:")
    console.log(bob.publicKey)

    console.log(`Do we have Bob's key yet?`)

    if (GPG.keysExist(bob.email)) {
        console.log('Yeah we do!')
    } else {
        console.log('Nope.')
        console.log('\n\nImporting Bobs public key.')
        sh.execSync(`gpg --import`, { input: bob.publicKey })
    
        console.log('\n\nIs it there now?')
        if (!GPG.keysExist(bob.email)) throw "Nope."

        console.log('Yes!')
    }
    

    //* Encrypt message for Bob:
    let bobsMessage = GPG.encrypt("Hey Bob, it's Alice", bob.email)

    GPG.decrypt(bobsMessage, 'B_SecretPassphrase') // => "Hey Bob, it's Alice"
}


/*

const { GPG, OpenPGP } = require('@webkit.ipfs/security')
const Path = require('path')
const HOME = process.env.HOME


async function test () {
   let alice = {
      name: 'Alice',
      email: 'alice@my.org'
   }
   
   let bob = {
      name: 'Bob',
      email: 'bob@my.org'
   }
   
   //* Does the key exist?
   GPG.keysExist(alice.email) // => false
   GPG.keysExist(bob.email) // => false
   
   //* Create keys for Alice (Ultimately trusted by GPG):
   GPG.generateKeys(alice.email, 'A_SecretPassphrase')
   
   bob = {
      ...bob,
      ...(await OpenPGP.generateKeys(bob))
   }
   
   //* Import Bob's public key:
   GPG.importPublicKey(bob.publicKey)
   
   
   //* Authenticate key:
   GPG.authenticate(alice, 'BadGuess') // => false
   GPG.authenticate(alice, 'A_SecretPassphrase') // => true
   
   //* Encrypt secrets:
   let secrets = fs.readFileSync('secrets.txt')
   let encrypted = GPG.encrypt(secrets, alice)
   GPG.decrypt(encrypted, 'A_SecretPassphrase') // => "Alice's secrets"
   
   //* Encrypt message for recipient:
   let bobsMessage = GPG.encrypt("Hey Bob, it's Alice", bob)
   GPG.decrypt(bobsMessage, 'B_SecretPassphrase') // => "Hey Bob, it's Alice"
   
}

*/
