### What is [PGP, GnuPG, and OpenPGP?](https://askubuntu.com/questions/186805/difference-between-pgp-and-gpg)

**Pretty Good Privacy** (PGP) was originally written by Phil Zimmermann, and is now owned by Symantec. The formats for keys, encrypted messages and message signatures were defined by that software. These have now been formalised as the **OpenPGP standard**.

The **GNU Privacy Guard**, GnuGP (GPG) software is an independent implementation of the OpenPGP standards, so you can use it to exchange encrypted messages with people using other OpenPGP implementations . . 

Due to GnuGP's popularity on Linux systems, it is also fairly common for people to incorrectly use the term "GPG" to refer to the whole OpenPGP cryptography system (e.g. "GPG keys" or "GPG signatures"). It is usually pretty clear what they mean from the context though.


## GnuPrivacyGuard

See this exceptional [tutorial on gpg](https://linux.101hacks.com/unix/gpg-command-examples/).

[GPG Configuration Options](https://www.gnupg.org/documentation/manuals/gnupg-devel/GPG-Configuration-Options.html)

> Anyway, you should never pass the passphrase as parameter (you're using the wrong argument anyway, have a look at the --passphrase... options in the GnuPG man page): all other users on the system can read all processes' command lines including the secret passphrase! Instead, use `--passphrase-fd 0` and attach your application to GnuPG's STDIN (like you'd do with a pipe on the command line), and pass the passphrase as input.


### Simple usage of `gpg`:
```bash

keys="$HOME/.gpg"
uid=$USER
gpg_id="user@my.org"
pw='secret'


# Quick Generate Key:
gpg --batch --passphrase $pw --quick-gen-key $gpg_id

# Quick Generate Key (safely):
echo $pw | gpg --batch --passphrase-fd 0 --quick-gen-key $gpg_id


# Select Fingerprint of Public Key:
set fingerprint (string trim  $(gpg --list-keys | head -4 | tail -1))
gpg --fingerprint $gpg_id

# Set to never expire:
gpg --quick-set-expire $fingerprint never

# List keys:
gpg --list-keys
gpg --list-secret-keys

# List key:
gpg --list-keys $gpg_id
gpg --list-secret-keys $gpg_id


# Encrypt/Decrypt private file:
gpg --encrypt --sign --armor -r $gpg_id $file
gpg --decrypt $file

# (--passphrase-file)
gpg -d --pinentry-mode loopback --passphrase-file pass.txt < encrypted.asc

echo $pw | gpg --batch --passphrase-fd 0 --output private.pgp --armor --export-secret-key steward@webkit.ipfs


   # without interaction (pin-entry mode):
   gpg --pinentry-mode loopback --passphrase=$secret --encrypt --sign --armor -r $gpg_id $filename
   gpg --decrypt joke.txt.asc > /dev/null 2>&1
   echo $pw | gpg --batch --passphrase-fd 0 --decrypt $file


# Export keys:
gpg --output public.asc.pgp --export $gpg_id
gpg --output private.asc.pgp --export-secret-key $gpg_id

   # Export ASCII-encrypted keys:
   gpg --output public.asc.pgp --armor --export $gpg_id
   gpg --output private.asc.pgp --armor --export-secret-key $gpg_id

   # Export keys without interaction (safely):
   echo $pw | gpg --batch --passphrase-fd 0 --output public.pgp --armor --export $gpg_id
   echo $pw | gpg --batch --passphrase-fd 0 --output private.pgp --armor --export-secret-key $gpg_id

   # Export keys without interaction (pin-entry mode):
   gpg --pinentry-mode loopback --passphrase=$pw --output public.pgp --armor --export $gpg_id
   gpg --pinentry-mode loopback --passphrase=$pw --output private.pgp --armor --export-secret-key $gpg_id

# Import friend's public keys:
gpg --import friend.pgp

# Send friend an encrypted file:
gpg --recipient friend --encrypt filename

# Send friend an ASCII-encrypted file:
gpg --recipient bob --armor --encrypt filename

# Sign Git commit:
git add <path/to/file>
git commit --gpg-sign --signoff -m 'commit message'

# Show key signature:
git log --show-signature -1


# Import keys:
gpg --import public.pgp; gpg --import private.pgp
  # Trust private key "ultimately":
expect -c "spawn gpg --edit-key $fingerprint trust quit; send \"never\"; expect eof"

# Delete keys:
gpg --delete-secret-key $gpg_id
gpg --delete-key $gpg_id





# Encrypt/Decrypt file
   # (manual passphrase only):
      #? Encrypt file:
      gpg -c test.txt
      rm test.txt
      #? Decrypt file:
      gpg test.txt
      cat test.txt

   # (pinentry mode, exported key + pw):
      gpg --pinentry-mode loopback --passphrase=$pw ./path/to/private.pgp
```
