# Webkit Security Package:

Accessories for security utilities:
 * GnuPrivacyGuard
 * OpenSSL

Provides convenient abstractions:
 * CredentialsManager
 * CertificateAuthority


## Installation:
```bash
yarn add https://gitlab.com/webkit.ipfs/security.git

# yarn add @webkit.ipfs/security
```



## GPG (GnuPrivacyGuard):

[GPG Module usage](./docs/GPG.md)

## CredentialsManager usage:

[CredentialsManager usage](./docs/CredentialsManager.md)

## CertificateAuthority usage:

[CertificateAuthority usage](./docs/CertificateAuthority.md)

